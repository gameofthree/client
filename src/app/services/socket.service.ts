import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class SocketService {

  private eventConsumers: Map<string, (msg: any, ws: WebSocket) => void> = new Map();

  private ws = new WebSocket(environment.webSocketUrl);

  private maxRetries = 20;

  subscribe<T>(channel: string, consumer: (msg: T, ws: WebSocket) => void): this {
    this.eventConsumers.set(channel, consumer);
    this.reattachMessageHandler();

    return this;
  }

  unsubscribe(channel: string) {
    if (this.eventConsumers.has(channel)) {
      this.eventConsumers.delete(channel);
      this.reattachMessageHandler();
    }
  }

  send(channel: string, payload: any = ''): void {
    let retries = 0;
    const intervalId = setInterval(() => {
      if (retries >= this.maxRetries) {
        clearInterval(intervalId);
        return;
      }

      if (this.ws.readyState === this.ws.CONNECTING) {
        retries++;
        return;
      }

      if (this.ws.readyState === this.ws.CLOSED || this.ws.readyState === this.ws.CLOSING) {
        this.ws = new WebSocket(environment.webSocketUrl);
        retries++;
        return;
      }

      this.ws.send(`${channel}:${payload}`);
      clearInterval(intervalId);
    }, 1000);
  }

  private reattachMessageHandler() {
    this.ws.onmessage = e => {
      const pair = JSON.parse(e.data) as { channel: string, payload: object };

      if (this.eventConsumers.has(pair.channel)) {
        this.eventConsumers.get(pair.channel)(pair.payload, this.ws);
      }
    };
  }
}
