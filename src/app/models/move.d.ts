import {Player} from './player';

export interface Move {
  id: string;
  player: Player;
  lastNumber: number;
  modifier: number;
  createdOn: string;
}
