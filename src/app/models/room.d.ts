import {Player} from './player';
import {Move} from './move';

export interface Room {
  id: string;
  opponent: Player;
  currentSide: Player;
  winner: Player;
  history: Move[];
  idle: boolean;
}
