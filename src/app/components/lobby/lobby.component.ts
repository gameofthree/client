import {Component, OnInit} from '@angular/core';
import {SocketService} from '../../services/socket.service';
import {Room} from '../../models/room';

const TURN_TIME = 60;

@Component({
  selector: 'gameofthree-lobby',
  templateUrl: './lobby.component.html'
})
export class LobbyComponent implements OnInit {
  public disabled = false;
  public playerId: string = null;
  public room: Room;
  public modifier = 0;
  public autoplay = true;
  public turnTimeLeft = TURN_TIME;


  constructor(private socketService: SocketService) {
  }

  ngOnInit(): void {
  }

  toggleAutoPlay($event) {
    this.autoplay = $event.target.checked;
    this.tryAutoMove();
  }

  subscribe() {
    if (this.disabled) {
      return;
    }

    this.disabled = true;
    this.play();

    this.socketService.send('JOIN_ROOM', 1);
    const intervalId = setInterval(() => {
      if (!this.room) {
        return;
      }

      if (--this.turnTimeLeft === 0) {
        this.socketService.send('IDLE_GAME', 1);
        this.room.idle = true;
        this.room.winner = this.room.opponent;
      }

      if (this.isIdle() || this.hasWinner()) {
        clearInterval(intervalId);
        this.turnTimeLeft = 0;
      }
    }, 1000);
  }

  isWaiting(): boolean {
    return this.room?.currentSide && !this.room?.opponent;
  }

  isPlaying(): boolean {
    return this.room?.currentSide && this.room?.opponent && !this.room?.winner;
  }

  hasWinner(): boolean {
    return !!this.room?.winner;
  }

  isIdle(): boolean {
    return this.room?.idle;
  }

  isYourTurn(): boolean {
    return this.room?.currentSide.id === this.playerId;
  }

  move() {
    this.socketService.send('MOVE', this.modifier);
  }

  getModifier(): number {
    if (!this.room) {
      throw new Error();
    }

    const lastMove = this.room.history[this.room.history.length - 1];
    const result = (lastMove.lastNumber + lastMove.modifier) / 3;

    for (let i = -1; i <= 1; i++) {
      const newResult = Math.floor((result + i) / 3);

      if ((newResult * 3) - i === result) {
        return i;
      }
    }

    return 0;
  }

  private play() {
    this.tryAutoMove();

    this.socketService
      .subscribe<Room>(
        'MOVE_FINISHED',
        room => ([this.room, this.turnTimeLeft] = [room, TURN_TIME]) && this.tryAutoMove()
      )
      .subscribe<Room>(
        'ROOM_JOINED',
        room => ([this.room, this.playerId] = [room, this.playerId || room.currentSide.id]) && this.tryAutoMove()
      )
      .subscribe<string>(
        'BAD_REQUEST',
        exception => exception === 'InvalidMoveException' && alert('Invalid move. Try again!')
      );
  }

  private tryAutoMove() {
    if (this.isYourTurn() && this.room.opponent && this.autoplay && !this.room.winner && !this.room.idle) {
      this.modifier = this.getModifier();
      this.move();
    }
  }

  reset() {
    this.playerId = null;
    this.room = null;
    this.modifier = 0;
    this.disabled = false;
    this.subscribe();
  }
}
