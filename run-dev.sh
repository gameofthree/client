#!/bin/sh

PORT="80"
if [ -n "$1" ]; then
  PORT="$1"
fi

IMAGE_NAME="gameofthree-client"
if [ -n "$2" ]; then
  IMAGE_NAME="$2"
fi

CONTAINER_NAME="$IMAGE_NAME"

docker container rm -f "$CONTAINER_NAME"
docker image rm -f "$CONTAINER_NAME"

docker network create --driver bridge reverse_proxy_network || true
docker build -t "$IMAGE_NAME" --build-arg PORT="$PORT" -f ./Dockerfile-dev .
export MSYS_NO_PATHCONV=1
docker run --network reverse_proxy_network --expose "$PORT" -e PORT="$PORT" -v "$(pwd)/src/:/app/src/" --name "$CONTAINER_NAME" "$IMAGE_NAME"
