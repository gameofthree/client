#!/bin/sh

PORT="80"
if [ -n "$1" ]; then
  PORT="$1"
fi

REVERSE_PROXY_HOST="localhost"
if [ -n "$2" ]; then
  REVERSE_PROXY_HOST="$2"
fi

IMAGE_NAME="gameofthree-client"
if [ -n "$3" ]; then
  IMAGE_NAME="$3"
fi

CONTAINER_NAME="$IMAGE_NAME"

docker container rm -f "$CONTAINER_NAME"
docker image rm -f "$IMAGE_NAME"

docker network create --driver bridge reverse_proxy_network || true
docker build -t "$IMAGE_NAME" --build-arg REVERSE_PROXY_HOST="$REVERSE_PROXY_HOST" .
docker run --network reverse_proxy_network --expose "$PORT" -e PORT="$PORT" -e REVERSE_PROXY_HOST="$REVERSE_PROXY_HOST" --name "$CONTAINER_NAME" "$IMAGE_NAME"
