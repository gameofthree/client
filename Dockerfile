FROM node:current-alpine3.12 as app-build

RUN mkdir -p /app
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .

ARG REVERSE_PROXY_HOST
ENV REVERSE_PROXY_HOST=$REVERSE_PROXY_HOST

RUN apk add gettext
RUN envsubst < ./conf/environment.prod.ts.template > ./src/environment.prod.ts

RUN npm install -g @angular/cli
RUN ng build --prod

FROM nginx:latest

COPY ./conf/default.conf.template /etc/nginx/templates/default.conf.template
COPY --from=app-build /app/dist/gameofthree-client/ /usr/share/nginx/html
