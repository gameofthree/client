# Gameofthree Client
An angular client which connects through WebSocket and is the main terminal for the user.

# Starting
There are two docker files - `Dockerfile` and `Dockerfile-dev`, the first one makes a static production build and uses 
`nginx` to serve the content from the public html directory. The latter uses the build-in web-server from `ng serve` and 
mounts the project folder in favor of live reload.

These files can be run via `run.sh` and `run-dev.sh` respectively.


